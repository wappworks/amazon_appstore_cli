# Amazon Appstore Command Line Interface (CLI)
-----

## Overview
This is a CLI to help developers manage/automate their Amazon appstore submissions.

## Installation
To install the tool from the Git repo directory:

```
pip install git+https://bitbucket.org/wappworks/amazon_appstore_cli.git#egg=amazon-appstore-cli
```

## Usage
Run `amazon_appstore_cli` to see the help documentation,

## License
This tool is released under the [MIT license](LICENSE.md).

## The Development Environment
Install this project and library in PIP editable mode for reduced development and test cycles. 

Run: 
```
    pip install -e .
```

To uninstall:
```
    pip uninstall amazon-appstore-cli
```