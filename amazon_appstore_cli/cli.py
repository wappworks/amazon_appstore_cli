import click
from app_editor import AmazonAppEditor


@click.group(chain=True)
@click.option('--id', required=True, help="The Appstore API profile ID")
@click.option('--secret', required=True, help="The Appstore API profile secret")
@click.option('--app', required=True, help="The target Amazon app ID")
@click.pass_context
def cli(ctx, id, secret, app):
    ctx.obj = AmazonAppEditor(id, secret, app, False)


@cli.command()
@click.option('--apk-path', required=True, help="Path to the APK file to upload to the listing")
@click.pass_obj
def upload_apk(app_editor, apk_path):
    try:
        click.echo("Queuing the APK upload - %s" % apk_path)

        app_editor.begin()
        app_editor.update_apk(apk_path)
    except Exception as e:
        raise click.ClickException(e.message)


@cli.command()
@click.pass_obj
def commit(app_editor):
    try:
        click.echo("Committing the edits...")

        if app_editor.is_editing:
            app_editor.end()

        click.echo("Commit complete.")
    except Exception as e:
        raise click.ClickException(e.message)
