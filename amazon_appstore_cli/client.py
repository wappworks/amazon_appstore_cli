import requests
import copy
from exception_manager import ExceptionManager


# noinspection PyBroadException
class AmazonAppstoreClient(ExceptionManager):
    API_VERSION = "v1"
    API_BASE_URL = 'https://developer.amazon.com/api/appstore'

    AUTH_URL = "https://api.amazon.com/auth/o2/token"

    def __init__(self, profile_id, profile_secret, app_id, quiet_exceptions=True):
        super(AmazonAppstoreClient, self).__init__(quiet_exceptions)

        self._profile_id = profile_id
        self._profile_secret = profile_secret
        self._app_id = app_id
        self._auth_header = None
        self._app_base_url = "{}/{}/applications/{}".format(self.API_BASE_URL, self.API_VERSION, app_id)

    def authenticate(self, forced=False):
        # Already authenticated? We're done!
        if self._auth_header and not forced:
            return True

        auth_response = requests.post(self.AUTH_URL, data={
            "grant_type": "client_credentials",
            "client_id": self._profile_id,
            "client_secret": self._profile_secret,
            "scope": "appstore::apps:readwrite",
        })

        if auth_response.status_code != 200:
            self._signal_exception("Authentication HTTP request failed ({}): {}".format(auth_response.status_code, auth_response.text))
            return False

        auth_token = auth_response.json().get("access_token")
        if not auth_token:
            self._signal_exception("Authentication failed: Missing access token - {}".format(auth_response.text))
            return False

        self._auth_header = {'Authorization': "Bearer {}".format(auth_token)}
        return True

    def get(self, op_path, headers=None):
        if not self.authenticate():
            return None

        if headers:
            headers = copy.copy(headers)
            headers.update(self._auth_header)
        else:
            headers = self._auth_header

        return requests.get("{}/{}".format(self._app_base_url, op_path), headers=headers)

    def delete(self, op_path, headers=None):
        if not self.authenticate():
            return None

        if headers:
            headers = copy.copy(headers)
            headers.update(self._auth_header)
        else:
            headers = self._auth_header

        return requests.delete("{}/{}".format(self._app_base_url, op_path), headers=headers)

    def put(self, op_path, data, headers=None):
        if not data:
            return None

        if not self.authenticate():
            return None

        if headers:
            headers = copy.copy(headers)
            headers.update(self._auth_header)
        else:
            headers = self._auth_header

        return requests.put("{}/{}".format(self._app_base_url, op_path), data=data, headers=headers)

    def post(self, op_path, data=None, headers=None):
        if not self.authenticate():
            return None

        if headers:
            headers = copy.copy(headers)
            headers.update(self._auth_header)
        else:
            headers = self._auth_header

        return requests.post("{}/{}".format(self._app_base_url, op_path), data=data, headers=headers)