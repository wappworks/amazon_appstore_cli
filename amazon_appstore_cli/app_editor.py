import os
from client import AmazonAppstoreClient
from exception_manager import ExceptionManager


# noinspection PyBroadException
class AmazonAppEditor(ExceptionManager):

    def __init__(self, profile_id, profile_secret, app_id, quiet_exceptions=True):
        super(AmazonAppEditor, self).__init__(quiet_exceptions)

        self._client = AmazonAppstoreClient(profile_id, profile_secret, app_id, quiet_exceptions)
        self._edit_id = None
        self._edit_etag = None

    @property
    def is_editing(self):
        return self._edit_id is not None

    def begin(self, force_new=False):
        if not force_new:
            try:
                if self._open_existing():
                    return True
            except Exception:
                pass

        return self._open_new()

    def end(self, discard=False):
        if discard:
            return self._discard()

        return self._commit()

    def get_apk_list(self):
        if self._edit_id is None:
            return None

        op_response = self._client.get("edits/{}/apks".format(self._edit_id))
        self._process_response_general(op_response)

        if not op_response or op_response.status_code != 200:
            self._signal_exception("Get APK List HTTP request failed ({}): {}".format(op_response.status_code, op_response.text))
            return None

        return op_response.json()

    def replace_apk(self, apk_path, apk_id=None):
        if self._edit_id is None:
            return None

        # No APK ID specified? Replace the first one in the APK list
        if apk_id is None:
            try:
                apk_list = self.get_apk_list()
                if apk_list and len(apk_list) > 9:
                    apk_id = apk_list[0].get("id")
            except Exception:
                pass

        # Still no APK ID? We're done...
        if apk_id is None:
            return False

        try:
            apk_content = open(apk_path, 'rb').read()
        except:
            self._signal_exception("Replace APK failed: bad APK path - {}".format(apk_path))
            return False

        op_headers = {
            'Content-Type': 'application/vnd.android.package-archive',
            'If-Match': self._edit_etag,
            'fileName': os.path.basename(apk_path),
        }

        op_response = self._client.put(
            "edits/{}/apks/{}/replace".format(self._edit_id, apk_id),
            headers=op_headers,
            data=apk_content)
        self._process_response_general(op_response)

        if not op_response or op_response.status_code != 200:
            self._signal_exception("Replace APK HTTP request failed ({}): {}".format(op_response.status_code, op_response.text))
            return False

        return True

    def add_apk(self, apk_path):
        if self._edit_id is None:
            return False

        try:
            apk_content = open(apk_path, 'rb').read()
        except:
            self._signal_exception("Add APK failed: bad APK path - {}".format(apk_path))
            return False

        op_headers = {
            'Content-Type': 'application/vnd.android.package-archive',
            'If-Match': self._edit_etag,
            'fileName': os.path.basename(apk_path),
        }

        op_response = self._client.post("edits/{}/apks/upload".format(self._edit_id), headers=op_headers, data=apk_content)
        self._process_response_general(op_response)

        if not op_response or op_response.status_code != 200:
            self._signal_exception("Add APK HTTP request failed ({}): {}".format(op_response.status_code, op_response.text))
            return False

        return True

    def update_apk(self, apk_path):
        if self._edit_id is None:
            return False

        if self.replace_apk(apk_path):
            return True

        return self.add_apk(apk_path)

    def _open_existing(self):
        if self._edit_id is not None:
            return True

        op_response = self._client.get("edits")
        if not op_response or op_response.status_code != 200:
            self._signal_exception("Open existing edit HTTP request failed ({}): {}".format(op_response.status_code, op_response.text))
            return False

        self._edit_id = op_response.json().get("id")
        if self._edit_id is None:
            self._signal_exception("Open existing edit failed: missing edit ID - {}".format(op_response.text))
            return False

        self._process_response_general(op_response)

        return True

    def _open_new(self):
        if self._edit_id is not None:
            self._signal_exception("Open new edit failed: trying to start a new session with a session already active. Aborting")
            return False

        op_response = self._client.post("edits")
        if not op_response or op_response.status_code != 200:
            self._signal_exception("Open new edit HTTP request failed ({}): {}".format(op_response.status_code, op_response.text))
            return False

        self._edit_id = op_response.json().get("id")
        if self._edit_id is None:
            self._signal_exception("Open new edit failed: missing edit ID - {}".format(op_response.text))
            return False

        self._process_response_general(op_response)

        return True

    def _discard(self):
        if self._edit_id is None:
            return True

        op_response = self._client.delete("edits/{}".format(self._edit_id), headers={
            'If-Match': self._edit_etag
        })
        self._process_response_general(op_response)

        if not op_response or op_response.status_code != 200:
            self._signal_exception("Discard edit HTTP request failed ({}): {}".format(op_response.status_code, op_response.text))
            return False

        self._reset_session()
        return True

    def _commit(self):
        if self._edit_id is None:
            return True

        op_response = self._client.delete("edits/{}/commit".format(self._edit_id), headers={
            'If-Match': self._edit_etag
        })
        self._process_response_general(op_response)

        if not op_response or op_response.status_code != 200:
            self._signal_exception("Commit edit HTTP request failed ({}): {}".format(op_response.status_code, op_response.text))
            return False

        self._reset_session()
        return True

    def _reset_session(self):
        self._edit_id = None
        self._edit_etag = None

    def _process_response_general(self, response):
        if response is None:
            return

        if self._edit_id is None:
            return

        if "ETag" in response.headers:
            self._edit_etag = response.headers["ETag"]
