
class ExceptionManager(object):
    def __init__(self, quiet_exceptions):
        self.__quiet_exceptions = quiet_exceptions

    def _signal_exception(self, message):
        if self.__quiet_exceptions:
            return

        raise Exception(message)